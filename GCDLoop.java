public class GCDLoop {
	public static void main(String[] args) {
		int a =Integer.parseInt(args[0]);
		int b =Integer.parseInt(args[1]);
		
		System.out.println(gcd(a > b ? a : b ,a > b ? b : a));
	}

	private static int gcd(int a, int b) {
		int GCD = 0;
		int r;
		
		do {
			r = a % b;
			a=b;
			b=r;
			GCD=a;
		}while(r!=0);
			
		return GCD;
				
	}

}